package cn.ghb.main.servlet;

import cn.ghb.main.service.MainService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author 高洪滨
 * @since 2021/09/07
 */
public class MainServlet extends HttpServlet {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private MainService mainService;

    @Override
    public void init() throws ServletException {
        mainService = new MainService();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info(mainService.main());
        resp.sendRedirect("main.jsp");
    }

}
