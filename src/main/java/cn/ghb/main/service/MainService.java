package cn.ghb.main.service;

import java.util.logging.Logger;

/**
 * @author 高洪滨
 * @since 2021/09/07
 */
public class MainService {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @SuppressWarnings("unused")
    public String main() {
        logger.info("im in " + getClass().getCanonicalName());
        return "Welcome";
    }

}
